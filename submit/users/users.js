const USERS = 'users';
const DEFAULT_USERS = './users';
const DEFAULT_INDEXES = { _id: 'text'};

function users(db) {
  this.db = db;
  this.users = db.collection(USERS);
}	

//this will find the user against a specific id
users.prototype.find = function(query) {
  const searchSpec = { "_id": query };
  return this.users.find(searchSpec).toArray();
  
}


//this will delete against a specific id
users.prototype.delete = function (query) {
    const searchSpec = { "_id": query };
    return this.users.deleteOne(searchSpec).
        then(function (results) {
            return new Promise(function (resolve, reject) {
                if (results.deletedCount === 1) {
                    resolve();
                }
                else {
                    reject(new Error(`cannot delete user ${id}`));
                }
            });
        });
}

//this will process put request
users.prototype.put = function (request) {
    return this.users.insert(request.body).
        then(function (results) {
            return new Promise((resolve) => resolve(results.insertedId));
        });
}

//this will POST the user against a specific id
users.prototype.insert = function (query) {
    return this.users.insertOne(query).
        then(function (results) {
            return new Promise((resolve) => resolve(results.insertedId));
        });

}

users.prototype.post = function (request) {
    const srch= { "_id": request.params.id};
    return this.users.updateOne(srch,request.body).
        then(function (result) {
            return new Promise(function (resolve, reject) {
                if (result.modifiedCount != 1) {
                    reject(new Error(`updated ${result.modifiedCount} users`));
                }
                else {
                    resolve();
                }
            });
        });
}


module.exports = {
  Users: users
};
