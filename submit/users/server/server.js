const express = require('express');
const bodyParser = require('body-parser');


const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const SERVER_ERROR = 500;
const NO_CONTENT = 204;
const SEE_OTHER = 303;

function serve(port, model) {
    const app = express();
    //console.log("model is");
    app.locals.model = model;
    app.locals.port = port;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    setupRoutes(app);
    app.listen(port, function () {
        console.log(`listening on port ${port}`);
    });
}

function setupRoutes(app) {
    console.log("insideRoutes");
    app.get('/users/:id', findUser(app));
    app.delete('/users/:id', deleteUser(app));
    app.put('/users/:id', putUsers(app));
    app.post('/users/:id', updateUser(app));
}

function requestUrl(req) {
    const port = req.app.locals.port;
    return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}
  
module.exports = {
    serve: serve
}

//finds a user
function findUser(app) {
    return function (request, response) {
        const ID = request.params.id;
        console.log(ID);
        if (typeof ID === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            console.log(ID);
            request.app.locals.model.users.find(ID).
                then((results) => response.json(results)).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(SERVER_ERROR);
                });
        }
    };
}

//deletes a user
function deleteUser(app) {
    return function (request, response) {
        const ID = request.params.id;
        if (typeof ID === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.delete(ID).
                then((results) => response.json(results)).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(SERVER_ERROR);
                });
        }
    };
}

function putUsers(app) {
    return function (request, response) {
        //console.log(request.body);
        //let requestUserID = request.params.id;
        //let receivedID;
        request.app.locals.model.users.put(request).
            then(function (receivedID) {
                //receivedID = request.body[0]._id;
                //if (requestUserID !== receivedID) {
                //    response.append('Location', requestUrl(request) + '/' + receivedID);
                //    response.sendStatus(NO_CONTENT);
                //}
                //else {
                //    request.app.locals.model.users.insert(request).
                //        then(function () {
                //            response.append('Location', requestUrl(request) + '/' + receivedID);
                //            response.sendStatus(CREATED);
                //        }).
                //        catch((err) => {
                //            console.error(err);
                //            response.sendStatus(SERVER_ERROR);
                //        });
                //    //response.append('Location', requestUrl(request) + '/' + receivedID);
                //    //        response.sendStatus(CREATED);
                    
                //}
                response.append('Location', requestUrl(request) + '/' + receivedID);
                response.sendStatus(CREATED);
            }).
            catch((err) => {
                console.error(err);
                response.sendStatus(SERVER_ERROR);
            });
    };
}

//deletes a user
function updateUser(app) {
    return function (request, response) {
        const ID = request.params.id;
        if (typeof ID === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.post(request).
                then((results) => response.json(results)).
                catch((err) => {
                    console.error(err);
                    response.sendStatus(SEE_OTHER);
                });
        }
    };
}