'use strict';

const assert = require('assert');
const mongo = require('mongodb').MongoClient;

//used to build a mapper function for the update op.  Returns a
//function F = arg => body.  Subsequently, the invocation,
//F.call(null, value) can be used to map value to its updated value.
function newMapper(arg, body) {
  return new (Function.prototype.bind.call(Function, Function, arg, body));
}

//print msg on stderr and exit.
function error(msg) {
  console.error(msg);
  process.exit(1);
}

//export error() so that it can be used externally.
module.exports.error = error;


//auxiliary functions; break up your code into small functions with
//well-defined responsibilities.

//perform op on mongo db specified by url.
function dbOp(url, op) {
  //your code goes here
  mongo.connect(url, function(err, db) {
      if (err) throw err;
      var operations = JSON.parse(op);
      console.log(operations.op);
      //console.log(operations);
      switch (operations.op){
          case "create":
              insertRows(db, operations);
              break;
          case "read":
              readOrFindRows(db, operations);
              break;
          case "delete":
              deleteRow(db, operations);
              break;
          case "update":
              updateRow(db, operations);
              break;
      }
  });
}

function insertRows(db, operations){
    console.log(operations);
    var rowsToInsert = operations["args"];
    console.log(rowsToInsert);
    db.collection("test").insertMany(rowsToInsert, function(err, res) {
        if (err) throw err;
        console.log("documents inserted");
        db.close();
    });
}

function readOrFindRows(db, operations){
    if(operations.hasOwnProperty("args")){
        var rowToFind = operations["args"];
        console.log(rowToFind);
        findARow(db, rowToFind);
    }
    else{
        readAllRows(db)
    }
}

function findARow(db, rowToFind){
    db.collection("test").findOne(rowToFind, function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
}

function readAllRows(db){    
    db.collection("test").find({}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
}

function deleteRow(db, operations){
    var b = operations["args"]
    db.collection("test").deleteMany(operations["args"], function(err, obj) {
        if (err) throw err;   
        db.close();
    });
}

function updateRow(db, operations){
    var mapper = newMapper(operations["fn"][0],operations["fn"][1]);
    var valueToUpdate = operations["args"];
    var newvalues;
    var value;
    
    newvalues = mapper.call(null, valueToUpdate);
    console.log(valueToUpdate);
    console.log(newvalues);
    
    db.collection("test").updateOne(valueToUpdate, newvalues, function(err, res) {
        if (err) throw err;
        console.log("1 document updated");
        db.close();
    });
    
}

//make main dbOp() function available externally
module.exports.dbOp = dbOp;

